package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserDTO {

    private String fullName;
    private String email;
    private String password;
    private String matchingPassword;
    private String address;
    private String userRole;

}



