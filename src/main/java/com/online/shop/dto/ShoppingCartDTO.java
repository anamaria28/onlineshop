package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class ShoppingCartDTO {

    private List<ShoppingCartItemDTO> items;

    private String subTotal;

    private String total;

    public ShoppingCartDTO(){
        items = new ArrayList<>();
    }

    public void add(ShoppingCartItemDTO shoppingCartItemDTO){
        items.add(shoppingCartItemDTO);
    }
}

