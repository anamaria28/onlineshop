package com.online.shop.repository;

import com.online.shop.entities.ChosenProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChosenProductRepository extends JpaRepository<ChosenProduct, Integer> {
}

