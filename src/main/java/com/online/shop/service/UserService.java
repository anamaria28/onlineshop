package com.online.shop.service;

import com.online.shop.dto.UserDTO;
import com.online.shop.dto.UserDetailsDTO;
import com.online.shop.entities.User;
import com.online.shop.mapper.UserMapper;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public void addUser(UserDTO userDTO){
        User user = userMapper.map(userDTO);
        userRepository.save(user);
    }

    public UserDetailsDTO getUserDetailsDTOByEmail(String email){
        Optional<User> optionalUser = userRepository.findByEmail(email);
        User user = optionalUser.get();
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
        userDetailsDTO.setFullName(user.getFullName());
        userDetailsDTO.setAddress(user.getAddress());
        return userDetailsDTO;
    }
}





