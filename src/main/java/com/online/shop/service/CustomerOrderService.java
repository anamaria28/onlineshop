package com.online.shop.service;

import com.online.shop.entities.ChosenProduct;
import com.online.shop.entities.CustomerOrder;
import com.online.shop.entities.ShoppingCart;
import com.online.shop.entities.User;
import com.online.shop.repository.ChosenProductRepository;
import com.online.shop.repository.CustomerOrderRepository;
import com.online.shop.repository.ShoppingCartRepository;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerOrderService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ChosenProductRepository chosenProductRepository;

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    public void addCustomerOrder(String loggedInUserEmail, String shippingAddress){
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInUserEmail);

        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setUser(optionalUser.get());
        customerOrder.setShippingAddress(shippingAddress);
        customerOrderRepository.save(customerOrder);

        for(ChosenProduct chosenProduct : shoppingCart.getChosenProducts()){
            chosenProduct.setShoppingCart(null);
            chosenProduct.setCustomerOrder(customerOrder);
            chosenProductRepository.save(chosenProduct);
        }
    }

}

