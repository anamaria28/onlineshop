package com.online.shop.validator;

import com.online.shop.dto.UserDTO;
import com.online.shop.entities.User;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserValidator {

    @Autowired
    private UserRepository userRepository;

    public boolean isNotValidEmailAddress(String email) {
        String ePattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return !m.matches();
    }

    public boolean isNotValidPassword(String password) {
        String passPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,64}$";
        Pattern p = Pattern.compile(passPattern);
        Matcher m = p.matcher(password);
        return !m.matches();
    }

    public void validate(UserDTO userDTO, BindingResult bindingResult) {

        if(userDTO.getFullName() == null || userDTO.getFullName().isBlank()){
            FieldError fieldError = new FieldError("userDTO", "fullName", "Field can not be empty!");
            bindingResult.addError(fieldError);
        }

        if(userDTO.getEmail() == null || userDTO.getEmail().isBlank()){
            FieldError fieldError = new FieldError("userDTO", "email", "Field can not be empty!");
            bindingResult.addError(fieldError);
        } else if(isNotValidEmailAddress(userDTO.getEmail())){
            FieldError fieldError = new FieldError("userDTO", "email", "Please insert a valid email address!");
            bindingResult.addError(fieldError);
        }

        Optional<User> optionalUser = userRepository.findByEmail(userDTO.getEmail());
        if(optionalUser.isPresent()){
            FieldError fieldError = new FieldError("userDTO", "email", "Email is already in use!");
            bindingResult.addError(fieldError);
        }

        if(userDTO.getPassword() == null || userDTO.getPassword().isBlank()) {
            FieldError fieldError = new FieldError("userDTO", "password", "Field can not be empty!");
            bindingResult.addError(fieldError);
        }
//        else if(isNotValidPassword(userDTO.getPassword())){
//            FieldError fieldError = new FieldError("userDTO", "password", "Password needs to be between 8 and 64 characters long and must contain: One uppercase and lowercase letter, one digit and one special character!");
//            bindingResult.addError(fieldError);
//        }

        if(!userDTO.getPassword().equals(userDTO.getMatchingPassword())){
            FieldError fieldError = new FieldError("userDTO", "matchingPassword", "Passwords don't match!");
            bindingResult.addError(fieldError);
        }

        if(userDTO.getAddress() == null || userDTO.getAddress().isBlank()){
            FieldError fieldError = new FieldError("userDTO", "address", "Field can not be empty!");
            bindingResult.addError(fieldError);
        }
    }
}



