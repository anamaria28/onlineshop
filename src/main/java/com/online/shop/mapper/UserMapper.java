package com.online.shop.mapper;

import com.online.shop.dto.UserDTO;
import com.online.shop.entities.ShoppingCart;
import com.online.shop.entities.User;
import com.online.shop.enums.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User map(UserDTO userDTO){
        User user = new User();
        user.setFullName(userDTO.getFullName());
        user.setEmail(userDTO.getEmail());
        String passwordEncoded = bCryptPasswordEncoder.encode(userDTO.getPassword());
        user.setPassword(passwordEncoded);
        user.setAddress(userDTO.getAddress());
        user.setUserRole(UserRole.valueOf(userDTO.getUserRole()));
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUser(user);
        user.setShoppingCart(shoppingCart);
        return user;
    }

    public UserDTO map(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setFullName(user.getFullName());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setAddress(user.getAddress());
        userDTO.setUserRole(String.valueOf(user.getUserRole()));
        return userDTO;
    }
}

