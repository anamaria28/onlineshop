package com.online.shop.mapper;

import com.online.shop.dto.ProductDTO;
import com.online.shop.entities.Product;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class ProductMapper {

    public Product map(ProductDTO productDTO, MultipartFile multipartFile) {
        Product product = new Product();
        product.setName(productDTO.getName());
        product.setCategory(productDTO.getCategory());
        product.setPrice(Double.valueOf(productDTO.getPrice()));
        product.setDescription(productDTO.getDescription());
        product.setImage(convertToBytes(multipartFile));
        product.setQuantity(Integer.valueOf(productDTO.getQuantity()));
        return product;
    }

    private byte[] convertToBytes(MultipartFile multipartFile) {
        try{
            return multipartFile.getBytes();
        }catch (IOException exception){
            return new byte[0];
        }
    }

    public ProductDTO map(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName(product.getName());
        productDTO.setCategory(product.getCategory());
        productDTO.setPrice(String.valueOf(product.getPrice()));
        productDTO.setDescription(product.getDescription());
        productDTO.setImage(Base64.encodeBase64String(product.getImage()));
        productDTO.setId(String.valueOf(product.getId()));
        productDTO.setQuantity(String.valueOf(product.getQuantity()));
        return productDTO;
    }
}

